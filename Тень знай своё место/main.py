def make_shades(alley, k):
    shadesMap = [False] * len(alley)
    for m, h in enumerate(alley):
        if h == 0:
            continue
        elif k == 0:
            shadesMap[m] = bool(h)
            continue
        for i in range(m, m + k * h + (k // abs(k)), k // abs(k)):
            if i in range(len(shadesMap)):
                shadesMap[i] = True
            else:
                continue
    return shadesMap


def calculate_sunny_length(shades):
    return len(shades) - sum(shades)


def main():
    k = int(input())
    alley = [int(char) for char in input().split()]
    print('Тени достаточно' if calculate_sunny_length(make_shades(alley, k)) < 10 else 'Обгорел')
