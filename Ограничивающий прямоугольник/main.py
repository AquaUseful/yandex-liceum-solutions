class BoundingRectangle:
    def __init__(self):
        self.__xs = []
        self.__ys = []

    def add_point(self, x, y):
        self.__xs.append(x)
        self.__ys.append(y)

    def width(self):
        return abs(max(self.__xs) - min(self.__xs))

    def height(self):
        return abs(max(self.__ys) - min(self.__ys))

    def top_y(self):
        return max(self.__ys)

    def bottom_y(self):
        return min(self.__ys)

    def right_x(self):
        return max(self.__xs)

    def left_x(self):
        return min(self.__xs)

