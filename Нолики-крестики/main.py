size = int(input())
lst = [list(input()) for _ in range(size)]
win = '-'
for line in lst:
    if 'xxx' in ''.join(line):
        win = 'x'
    elif 'ooo' in ''.join(line):
        win = 'o'
for line in [[lst[x][y] for x in range(0, len(lst))] for y in range(len(lst[0]))]:
    if 'xxx' in ''.join(line):
        win = 'x'
    elif 'ooo' in ''.join(line):
        win = 'o'
print(win)
