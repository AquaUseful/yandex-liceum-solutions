class Thing:
    def __init__(self, kind, armor=0, strength=0, agility=0, intelligence=0):
        self._kind = kind
        self._armor = armor
        self._strength = strength
        self._agility = agility
        self._intelligence = intelligence

    def kind(self):
        return self._kind

    def armor(self):
        return self._armor

    def strength(self):
        return self._strength

    def agility(self):
        return self._agility

    def intelligence(self):
        return self._intelligence

    def copy(self):
        return Thing(self._kind, self._armor, self._strength, self._agility, self._intelligence)

    def __eq__(self, other):
        return ((self._kind == other.kind()) and (self._armor == other.armor()) and
                (self._strength == other.strength()) and (self._agility == other.agility()) and
                (self._intelligence == other.intelligence()))

    def __lshift__(self, other):
        if (self._armor - other) <= 0:
            return None
        return Thing(self._kind, self._armor - other, self._strength, self._agility, self._intelligence)

    def __ilshift__(self, other):
        if (self._armor - other) <= 0:
            self = None
        self._armor -= other
        return self

    def __add__(self, other):
        s, a, i = other
        return Thing(self._kind, self._armor, self._strength + s, self._agility + a, self._intelligence + i)

    def __iadd__(self, other):
        return self + other

    def __str__(self):
        return f'{self._kind} - Armor: {self._armor}, Strength: {self._strength}, ' \
            f'Agility: {self._agility}, Intelligence: {self._intelligence}'


class Player:
    def __init__(self):
        self._things = [None] * 7

    def armor(self):
        return sum(map(lambda el: 0 if el is None else el.armor(), self._things))

    def strength(self):
        return sum(map(lambda el: 0 if el is None else el.strength(), self._things))

    def agility(self):
        return sum(map(lambda el: 0 if el is None else el.agility(), self._things))

    def intelligence(self):
        return sum(map(lambda el: 0 if el is None else el.intelligence(), self._things))

    def put_on(self, thing):
        self._things[thing.kind()] = thing

    def take_off(self, kind):
        self._things[kind] = None

    def __add__(self, other):
        self.put_on(other.copy())

    def __sub__(self, other):
        self.take_off(other)

    def __lshift__(self, other):
        for i in range(len(self._things)):
            if self._things[i] is not None:
                self._things[i] = self._things[i] << other

    def __getitem__(self, kind):
        return self._things[kind]

    def __str__(self):
        print(self._things)
        return f'Player - \n\tArmor: {self.armor()},\n\tStrength: {self.strength()}\n\tAgility: ' \
            f'{self.agility()},\n\tIntelligence: {self.intelligence()}'

