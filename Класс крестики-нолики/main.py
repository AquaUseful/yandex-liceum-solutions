class TicTacToeBoard:
    def __init__(self):
        self.__board = [['-'] * 3 for _ in range(3)]
        self.__curr_player = 'X'

    def check_field(self):
        for board in (self.__board, [[self.__board[x][y] for x in range(3)] for y in range(3)]):
            for ch1, ch2, ch3 in board:
                if ch1 == ch2 == ch3 == '0' or ch1 == ch2 == ch3 == 'X':
                    return ch1
            if board[0][0] == board[1][1] == board[2][2] == 'X' or \
                    board[0][0] == board[1][1] == board[2][2] == '0' or \
                    board[0][2] == board[1][1] == board[2][0] == 'X' or \
                    board[0][2] == board[1][1] == board[0][2] == '0':
                return board[1][1]
        if self.__is_filled():
            return 'D'

    def get_field(self):
        return self.__board

    def new_game(self):
        self.__init__()

    def __swap_player(self):
        self.__curr_player = {'X': '0', '0': 'X'}[self.__curr_player]

    def __is_filled(self):
        for line in self.__board:
            if '-' in line:
                return False
        return True

    def __put_symb(self, row, col):
        self.__board[row][col] = self.__curr_player

    def __is_game_finished(self):
        return self.__is_filled() or self.check_field() is not None

    def __is_empty(self, row, col):
        return self.__board[row][col] == '-'

    def make_move(self, row, col):
        if self.__is_game_finished():
            return 'Игра уже завершена'
        row, col = row - 1, col - 1
        if not self.__is_empty(row, col):
            return f'Клетка {row + 1}, {col + 1} уже занята'
        self.__put_symb(row, col)
        self.__swap_player()
        if self.check_field() is None:
            return 'Продолжаем играть'
        elif self.check_field() == 'D':
            return 'Ничья'
        return f'Победил игрок {self.check_field()}'

