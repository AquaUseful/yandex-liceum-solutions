lines = []
while True:
    line = input()
    if not line:
        break
    lines.append(line.split(':'))
badPass = input().split(';')
print(*['To: ' + line[0] + '\n' + line[4].split(',')[0] +
        ', ваш пароль слишком простой, смените его.'
        for line in lines if line[1] in badPass], sep='\n')
