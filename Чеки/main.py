def export_check(text):
    from xlsxwriter import Workbook
    with Workbook('res.xlsx') as workbook:
        items = {}
        for line in (text + '\n---').split('\n'):
            if line == '---':
                worksheet = workbook.add_worksheet()
                for row, (name, price) in enumerate(sorted(items, key=lambda el: el[0])):
                    worksheet.write(row, 0, name)
                    worksheet.write(row, 1, price)
                    worksheet.write(row, 2, items[(name, price)])
                    worksheet.write(row, 3, '=B' + str(row + 1) + '*C' + str(row + 1))
                worksheet.write(row + 1, 0, 'Итого')
                worksheet.write(row + 1, 3, '=SUM(D1:D' + str(row + 1) + ')')
                items = {}
                continue
            name, price, count = line.split('\t')
            if (name, int(price)) in items:
                items[(name, int(price))] += int(count)
            else:
                items[(name, int(price))] = int(count)

