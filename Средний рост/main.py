from sys import stdin
data = tuple(map(int, stdin))
print(sum(data) / len(data) if data else -1)
