class Date:
    def __init__(self, year, month, day):
        self.set_year(year)
        self.set_month(month)
        self.set_day(day)

    def set_year(self, year):
        self.__year = year

    def set_month(self, month):
        self.__month = month

    def set_day(self, day):
        self.__day = day

    def get_year(self):
        return self.__year

    def get_month(self):
        return self.__month

    def get_day(self):
        return self.__day


class AmericanDate(Date):
    def format(self):
        return f'{str(self.get_month()).zfill(2)}.{str(self.get_day()).zfill(2)}.{self.get_year()}'


class EuropeanDate(Date):
    def format(self):
        return f'{str(self.get_day()).zfill(2)}.{str(self.get_month()).zfill(2)}.{self.get_year()}'

