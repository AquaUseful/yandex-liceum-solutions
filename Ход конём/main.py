def coords_to_num(coords):
    return ({'A': 1,
             'B': 2,
             'C': 3,
             'D': 4,
             'E': 5,
             'F': 6,
             'G': 7,
             'H': 8
             }[list(coords)[0]], int(list(coords)[1]))


def num_to_coords(numcoords):
    return {1: 'A',
            2: 'B',
            3: 'C',
            4: 'D',
            5: 'E',
            6: 'F',
            7: 'G',
            8: 'H'}[numcoords[0]] + str(numcoords[1])


def is_coord_in_desk(numcoords):
    return 0 < numcoords[0] < 9 and 0 < numcoords[1] < 9


def possible_turns(cell):
    cell = coords_to_num(cell)
    turns = [(cell[0] + el[0], cell[1] + el[1]) for el
             in ((1, 2), (2, 1), (-1, 2), (-2, 1), (-1, -2), (-2, -1), (1, -2), (2, -1))
             if is_coord_in_desk((cell[0] + el[0], cell[1] + el[1]))]
    return sorted([num_to_coords(turn) for turn in turns])
