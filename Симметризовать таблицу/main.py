size = int(input())
matrix = [[0] * size]
for lineNum in range(size - 1):
    line = input().split()
    line += [0] * (size - lineNum - 1)
    matrix.append(line)
for x in range(size):
    for y in range(size):
        matrix[x][y] = matrix[y][x]
for line in matrix:
    print(*line)
