def find_mountain(map):
    maxRow = maxCol = 0
    for row in range(len(map)):
        if map[row][map[row].index(max(map[row]))] > map[maxRow][maxCol]:
            maxRow, maxCol = row, map[row].index(max(map[row]))
    return maxRow, maxCol
