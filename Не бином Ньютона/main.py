seq = [int(input()) for _ in range(int(input()))]
print(*[sum(seq[pos:pos + 2]) for pos in range(len(seq) - 1)], sep='\n')
