names = [input() for _ in range(int(input()))]
k = int(input())
while names:
    print(*names[::k], sep='\n')
    names = [names[el] for el in range(len(names)) if el % k != 0]
