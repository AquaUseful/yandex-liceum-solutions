from itertools import count
from sys import stdin
cities = map(lambda tup: (tup[0], int(tup[2])),
             map(lambda line: line.strip().split(), stdin.readlines()))
cities_dict = {}
for city in cities:
    for interval in zip(count(0, 100), count(100, 100)):
        if (interval[0] * 1000) <= city[1] <= (interval[1] * 1000):
            if interval in cities_dict:
                cities_dict[interval].append(city[0])
                break
            else:
                cities_dict[interval] = [city[0]]
                break
for key in sorted(cities_dict):
    print(f'{key[0]} - {key[1]}: {", ".join(sorted(cities_dict[key]))}')

