data = input().split()
stack = []

ops = {
    '+': lambda x, y: y + x,
    '-': lambda x, y: y - x,
    '*': lambda x, y: y * x
}

for el in data:
    if el in ('+', '-', '*'):
        stack.append(ops[el](stack.pop(), stack.pop()))
    else:
        stack.append(int(el))
print(*stack)