def make_field(size):
    import numpy as np
    from itertools import cycle
    it1 = cycle((1, 0))
    it2 = cycle((0, 1))
    arr = cycle(([next(it1) for _ in range(size)], [next(it2) for _ in range(size)]))
    return np.array([next(arr) for _ in range(size)], dtype=np.int8).reshape(size, size)

