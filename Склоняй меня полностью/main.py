from pymorphy2 import MorphAnalyzer
word = MorphAnalyzer().parse(input())[0]
if word.tag.POS == 'NOUN':
    for num in ('sing', 'plur'):
        print({'sing': 'Единственное число:', 'plur': 'Множественное число:'}[num])
        for case in ('nomn', 'gent', 'datv', 'accs', 'ablt', 'loct'):
            print({'nomn': 'Именительный падеж:',
                   'gent': 'Родительный падеж:',
                   'datv': 'Дательный падеж:',
                   'accs': 'Винительный падеж:',
                   'ablt': 'Творительный падеж:',
                   'loct': 'Предложный падеж:'}[case],
                  word.inflect({num, case}).word)
else:
    print('Не существительное')
