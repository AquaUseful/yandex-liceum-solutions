def choose_coffee(preferences):
    global ingredients
    coffee = {
        'Эспрессо': (1, 0, 0),
        'Капучино': (1, 3, 0),
        'Маккиато': (2, 1, 0),
        'Кофе по-венски': (1, 0, 2),
        'Латте Маккиато': (1, 2, 1),
        'Кон Панна': (1, 0, 1)
    }
    for pref in preferences:
        if all([ingredients[i] >= el for i, el in enumerate(coffee[pref])]):
            ingredients = [ingredients[i] - el for i, el in enumerate(coffee[pref])]
            return pref
    return 'К сожалению, не можем предложить Вам напиток'
