from string import ascii_letters, digits
ALPHABET = ''.join((map(lambda el: '' if el in 'lI10oO' else el, ascii_letters + digits)))


def randpop(lst):
    from random import randrange
    return lst.pop(randrange(len(lst)))


def generate_password(m):
    alp = list(ALPHABET)
    return ''.join(map(lambda a: randpop(alp), range(m)))


def main(n, m):
    passwords = []
    while len(passwords) < n:
        password = generate_password(m)
        if password not in passwords:
            passwords.append(password)
    return passwords
