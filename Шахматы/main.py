WHITE = 'w'
BLACK = 'b'
DEBUG = True
# Команды отладочного режима:
# sw - передать ход другому игроку
#
# clr - очистить доску
#
# del [row] [col] - удалить фигуру на заданных координатах
#
# hs [row] [col] - выбрать фигуру на заданных координатах
# (выбирает любую фигуру, без проверок)
#
# hm [row] [col] - переместить выбранную фигуру на заданные координаты
# (перемещает любую фигуру в любую клетку, без проверок)
#
# mk [char] [row] [col] - создать фигуру текущего цвета на заданных координатах


def opponent(color):
    return {WHITE: BLACK, BLACK: WHITE}[color]


def check_coords(row, col):
    return 0 <= row < 8 and 0 <= col < 8


def check_line_diag(board, row, col, row1, col1):
    from itertools import repeat
    step_row = int(row1 > row) - int(row1 < row)
    step_col = int(col1 > col) - int(col1 < col)
    return all(map(lambda el: board.get_piece(el[0], el[1]) is None,
                   zip(range(row + step_row, row1, step_row) if step_row else repeat(row),
                       range(col + step_col, col1, step_col) if step_col else repeat(col))))


def check_line(row, col, row1, col1):
    return row == row1 or col == col1


def check_diag(row, col, row1, col1):
    return abs(row1 - row) == abs(col1 - col)


def print_board(board, marks=[]):
    print('     +----+----+----+----+----+----+----+----+')
    for row in range(7, -1, -1):
        print(' ', row, end='  ')
        for col in range(8):
            print('|', end='')
            cell = board.cell(row, col)
            print((' ## ' if cell == '  ' else f'#{cell}#') if (
                row, col) in marks else f' {cell} ', end='')
        print('|')
        print(f'     +----+----+----+----+----+----+----+----+')
    print(end='        ')
    for col in range(8):
        print(col, end='    ')
    print()


class Board:
    def __init__(self):
        self._color = WHITE
        self.put_default_pattern()

    def put_default_pattern(self):
        self.field = [[None] * 8 for _ in range(8)]
        self.field[0] = [
            Rook(WHITE), Knight(WHITE), Bishop(WHITE), Queen(WHITE),
            King(WHITE), Bishop(WHITE), Knight(WHITE), Rook(WHITE)
        ]
        self.field[1] = [
            Pawn(WHITE), Pawn(WHITE), Pawn(WHITE), Pawn(WHITE),
            Pawn(WHITE), Pawn(WHITE), Pawn(WHITE), Pawn(WHITE)
        ]
        self.field[6] = [
            Pawn(BLACK), Pawn(BLACK), Pawn(BLACK), Pawn(BLACK),
            Pawn(BLACK), Pawn(BLACK), Pawn(BLACK), Pawn(BLACK)
        ]
        self.field[7] = [
            Rook(BLACK), Knight(BLACK), Bishop(BLACK), Queen(BLACK),
            King(BLACK), Bishop(BLACK), Knight(BLACK), Rook(BLACK)
        ]

    def current_player_color(self):
        return self._color

    def cell(self, row, col):
        piece = self.field[row][col]
        return '  ' if piece is None else {WHITE: 'w', BLACK: 'b'}[piece.get_color()] + piece.char()

    def get_piece(self, row, col):
        return self.field[row][col] if check_coords(row, col) else None

    def check_piece_move(self, row, col, row1, col1):
        piece = self.get_piece(row, col)
        piece1 = self.get_piece(row1, col1)
        return check_coords(row, col) and check_coords(row1, col1) and \
            (row != row1 or col != col1) and \
            piece is not None and \
            piece.get_color() == self._color and \
            not isinstance(piece1, King) and \
            ((piece1 is None and piece.can_move(self, row, col, row1, col1)) or
             (piece1 is not None and piece1.get_color() == opponent(self._color) and
              piece.can_attack(self, row, col, row1, col1)))

    def check_piece_attack(self, row, col, row1, col1):
        piece = self.get_piece(row, col)
        if isinstance(piece, King) and piece.get_color() == self._color:
            for row_shift in (-1, 0, 1):
                for col_shift in (-1, 0, 1):
                    if row_shift == col_shift == 0:
                        continue
                    if row1 == row + row_shift and col1 == col + col_shift:
                        return True
        return check_coords(row, col) and check_coords(row1, col1) and \
            (row != row1 or col != col1) and \
            piece is not None and \
            piece.get_color() == self._color and \
            piece.can_attack(self, row, col, row1, col1)

    def move_piece(self, row, col, row1, col1, check_disabled=False):
        piece = self.get_piece(row, col)
        if check_disabled or self.check_piece_move(row, col, row1, col1):
            self.field[row][col] = None
            self.field[row1][col1] = piece
            piece.move()
            return True
        return False

    def castling0(self):
        row = {WHITE: 0, BLACK: 7}[self._color]
        piece1 = self.get_piece(row, 0)
        piece2 = self.get_piece(row, 4)
        if isinstance(piece1, Rook) and piece1.get_color() == self._color and \
                piece1.is_first_move() and self.check_piece_move(row, 0, row, 3) and \
                isinstance(piece2, King) and piece2.get_color() == self._color and \
                piece2.is_first_move():
            self.move_piece(row, 0, row, 3, True)
            self.move_piece(row, 4, row, 2, True)
            return True
        return False

    def castling7(self):
        row = {WHITE: 0, BLACK: 7}[self._color]
        piece1 = self.get_piece(row, 7)
        piece2 = self.get_piece(row, 4)
        if isinstance(piece1, Rook) and piece1.get_color() == self._color and \
                piece1.is_first_move() and self.check_piece_move(row, 7, row, 5) and \
                isinstance(piece2, King) and piece2.get_color() == self._color and \
                piece2.is_first_move():
            self.move_piece(row, 7, row, 5, True)
            self.move_piece(row, 4, row, 6, True)
            return True
        return False

    def promote_pawn(self, row, col, new_piece):
        piece = self.get_piece(row, col)
        if (row == 0 or row == 7) and isinstance(piece, Pawn) and piece.get_color() == self._color:
            self.field[row][col] = new_piece(self._color)

    def swap_color(self):
        self._color = opponent(self._color)

    def cells_under_attack(self):
        self.swap_color()
        atk = set((row1, col1)
                  for row in range(8)
                  for col in range(8)
                  for row1 in range(8)
                  for col1 in range(8)
                  if self.check_piece_attack(row, col, row1, col1))
        self.swap_color()
        return atk

    def promotable_pawn(self):
        row = {WHITE: 7, BLACK: 0}[self._color]
        for col in range(8):
            if isinstance(self.get_piece(row, col), Pawn):
                return (row, col)
        return False

    def possible_moves_for_piece(self, row, col):
        if row is None or col is None:
            return []
        return [(row1, col1)
                for row1 in range(8)
                for col1 in range(8)
                if self.check_piece_move(row, col, row1, col1)]

    def piece_can_be_selected(self, row, col):
        piece = self.get_piece(row, col)
        return piece is not None and piece.get_color() == self._color


class ChessPiece:
    def __init__(self, color):
        self._color = color
        self._first_move = True

    def get_color(self):
        return self._color

    def char(self):
        return self.__class__.__name__[0]

    def move(self):
        self._first_move = False

    def can_move(self, board, row, col, row1, col1):
        pass

    def can_attack(self, board, row, col, row1, col1):
        return self.can_move(board, row, col, row1, col1)

    def is_first_move(self):
        return self._first_move

    def reset_first_move(self):
        self._first_move = True


class Pawn(ChessPiece):
    def can_move(self, board, row, col, row1, col1):
        direction = 1 if self._color == WHITE else -1
        return col == col1 and \
            (row1 == row + direction or
             (row1 == row + direction * 2 and self._first_move and
              board.get_piece(row + direction, col) is None))

    def can_attack(self, board, row, col, row1, col1):
        direction = 1 if self._color == WHITE else -1
        return row + direction == row1 and (col1 == col + 1 or col1 == col - 1)


class Knight(ChessPiece):
    def char(self):
        return 'N'

    def can_move(self, board, row, col, row1, col1):
        for row_shift in (-2, -1, 1, 2):
            for col_shift in (-2, -1, 1, 2):
                if abs(row_shift) == abs(col_shift):
                    continue
                if row1 == row + row_shift and col1 == col + col_shift:
                    return True


class Queen(ChessPiece):
    def can_move(self, board, row, col, row1, col1):
        return (check_line(row, col, row1, col1) or check_diag(row, col, row1, col1)) \
            and check_line_diag(board, row, col, row1, col1)


class Rook(ChessPiece):
    def can_move(self, board, row, col, row1, col1):
        return check_line(row, col, row1, col1) and check_line_diag(board, row, col, row1, col1)


class Bishop(ChessPiece):
    def can_move(self, board, row, col, row1, col1):
        return check_diag(row, col, row1, col1) and check_line_diag(board, row, col, row1, col1)


class King(ChessPiece):
    def can_move(self, board, row, col, row1, col1):
        for row_shift in (-1, 0, 1):
            for col_shift in (-1, 0, 1):
                if row_shift == col_shift == 0:
                    continue
                if row1 == row + row_shift and col1 == col + col_shift:
                    return (row1, col1) not in board.cells_under_attack()


def main():
    board = Board()
    s_row = s_col = None
    while True:
        print_board(board, board.possible_moves_for_piece(s_row, s_col))
        print('Ход игрока: ' + board.current_player_color())
        print('Доступные команды: ')
        print('s [row] [col] - выбрать фигуру для хода (при выборе будут показаны доступные ходы, '
              'нельзя выбрать фигуру без доступных ходов)')
        print(
            'm [row] [col] - сделать ход фигурой (только если есть выбранная фигура)')
        print(
            'c [0/7] - выполнить рокировку для ладьи в 0 или 7 столбце (если возможна)')
        print('x - выход')
        print('rst - начать игру заново')
        command = input('> ').split(' ')
        cmd = command[0]
        args = tuple(map(int, command[1:]))
        if cmd == 'x':
            break
        elif cmd == 'rst':
            board = Board()
            s_row = s_col = None
        elif cmd == 's':
            if not args:
                s_row = s_col = None
                continue
            if board.piece_can_be_selected(args[0], args[1]) and \
                    board.possible_moves_for_piece(args[0], args[1]):
                s_row, s_col = args[0], args[1]
            else:
                print('Клетка пуста или фигура не может быть выбрана!')
        elif cmd == 'm':
            if s_row is None or s_col is None:
                print('Фигура не выбрана!')
                continue
            fm = board.get_piece(s_row, s_col).is_first_move()
            if board.move_piece(s_row, s_col, args[0], args[1]):
                if any(map(lambda piece: isinstance(piece, King) and
                           piece.get_color() == board.current_player_color(),
                           map(lambda c: board.get_piece(c[0], c[1]), board.cells_under_attack()))):
                    print('Нельзя ставить короля под шах!')
                    board.move_piece(args[0], args[1], s_row, s_col, True)
                    if fm:
                        board.get_piece(s_row, s_col).reset_first_move()
                    continue
                prom_pawn = board.promotable_pawn()
                if prom_pawn:
                    print(
                        f'Пешка на координтах {prom_pawn} дошла до конца доски')
                    print(
                        'До какой фигуры повысить её (Q - ферзь, N - конь, B - слон, R - ладья)')
                    while True:
                        temp = input('Выберите фигуру: ')
                        if temp in ('Q', 'N', 'B', 'R'):
                            break
                    board.promote_pawn(*prom_pawn,
                                       {'Q': Queen, 'N': Knight, 'B': Bishop, 'R': Rook}[temp])
                board.swap_color()
            else:
                print('Данный ход невозможен!')
        elif cmd == 'c':
            if not {0: board.castling0(), 7: board.castling7()}[args[0]]:
                print('Рокировка невозможна!')
                continue
            board.swap_color()

        elif cmd == 'sw' and DEBUG:
            board.swap_color()
        elif cmd == 'del' and DEBUG:
            board.field[args[0]][args[1]] = None
        elif cmd == 'hm' and DEBUG:
            board.move_piece(s_row, s_col, args[0], args[1], True)
        elif cmd == 'hs' and DEBUG:
            s_row, s_col = args[0], args[1]
        elif 'mk' in cmd and DEBUG:
            board.field[args[0]][args[1]] = \
                {'Q': Queen, 'N': Knight, 'B': Bishop, 'R': Rook, 'K': King, 'P': Pawn}[
                    cmd[-1]](board.current_player_color())
        elif cmd == 'clr' and DEBUG:
            board.field = [[None] * 8 for _ in range(8)]
        else:
            print('Неизвестная команда!')


if __name__ == '__main__':
    main()
