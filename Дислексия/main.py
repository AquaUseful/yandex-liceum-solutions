from itertools import permutations
from sys import stdin
dic = input().lower().split()
for line in stdin:
    print(*map(lambda word: '#' * len(word) if any(map(
        lambda el: ''.join(el) in dic and ''.join(el) != word, permutations(word))) else word,
              line.lower().split()))

