h = 0
for n in range(int(input())):
    b = int(input())
    m = b // 256 ** 2
    r = b // 256 % 256
    h = 37 * (m + r + h) % 256
    b1 = h + r * 256 + m * 256 ** 2
    if h >= 100 or b != b1:
        print(n)
        break
else:
    print(-1)
