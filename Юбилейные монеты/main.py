from itertools import groupby
from sys import stdin
print(sum(map(lambda el: el[0][0] * (len(tuple(el[1])) - 1),
              groupby(sorted(map(lambda tup: (int(tup[0]), tup[1]),
                                 map(lambda line: line.strip().split(maxsplit=1),
                                     stdin.readlines())))))))
