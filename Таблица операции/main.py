def print_operation_table(operation, num_rows=9, num_columns=9):
    for ln in [[operation(x, y) for y in range(1, num_columns + 1)] for x in range(1, num_rows + 1)]:
        print(*ln, sep='\t')
