stats = []
stCount = int(input())
brother = [int(input()) for _ in range(stCount)]
stats = [brother[:], brother[:]]
for _ in range(int(input())):
    br, st = int(input()), int(input())
    stats[br - 1][st] = stats[br - 1][st] + int(input())
print(*stats[0])
print(*stats[1])
print(sum([1 for stat in range(len(stats[0])) if stats[0][stat] == stats[1][stat]]))
