count = int(input())
c = 0
titles = []
while c < count:
    titles.append([])
    while True:
        temp = input().split()
        if temp == ['*']:
            c += 1
            break
        titles[c].append('-'.join(temp) + '-')
print(*[''.join(title)[:-1] for title in titles[::-1]], sep=', ')

