class Summator:
    def transform(self, n):
        return n

    def sum(self, N):
        if N == 1:
            return 1
        return self.sum(N - 1) + self.transform(N)


class SquareSummator(Summator):
    def transform(self, n):
        return n * n


class CubeSummator(Summator):
    def transform(self, n):
        return n ** 3

