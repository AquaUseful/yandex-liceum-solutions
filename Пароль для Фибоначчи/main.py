def check_password(func):
    passed = False

    def wrapped_func(*args, **kwargs):
        nonlocal passed
        if passed:
            return func(*args, **kwargs)
        if input('Input password:') == 'password':
            passed = True
            return func(*args, **kwargs)
        print('Incorrect password')
        return
    return wrapped_func


@check_password
def fib(n):
    if n <= 2:
        return 1
    return fib(n - 1) + fib(n - 2)

