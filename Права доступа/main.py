perms = [input()[1:].split('/') for _ in range(int(input()))]
for _ in range(int(input())):
    path = input()[1:].split('/')
    for perm in perms:
        if len(path) < len(perm):
            continue
        flags = []
        for lv in range(len(perm)):
            if path[lv] != perm[lv]:
                flags.append(False)
                break
            else:
                flags.append(True)
        if all(flags):
            print('YES')
            break
    else:
        print('NO')
