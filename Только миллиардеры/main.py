strings = input().split(';')
print(*[','.join([money for money in string.split(',') if int(money) >= 10 ** 9])
        for string in strings], sep='\n')
