def gradient(color):
    from PIL import Image, ImageDraw
    color_add = {'R': 0, 'G': 1, 'B': 2}[color.upper()]
    img = Image.new('RGB', (512, 200), (0, 0, 0))
    color = [0, 0, 0]
    dr = ImageDraw.Draw(img)
    for x in range(0, 511, 2):
        dr.rectangle(xy=[(x, 0), (x + 2, 199)], fill=tuple(color))
        color[color_add] += 1
    img.save('res.png', 'PNG')
