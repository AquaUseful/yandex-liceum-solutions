print('')
string = input()
if 'хорош' in string:
    print('Отлично, у меня тоже всё хорошо :)')
elif 'плох' in string:
    print('Ничего, скоро всё наладится')
elif '?' in string or 'не' in string:
    print('Я вас не понимаю :(')
else:
    print('Ясно')