queue = []
for _ in range(int(input())):
    temp = input()
    if 'Следующий!' in temp:
        print('Заходит ' + queue.pop(0) + '!' if queue else 'В очереди никого нет.')
    elif 'Кто последний?' in temp:
        queue.append(temp[temp.find('Я - ') + 4:-1])
    elif 'Я только спросить!' in temp:
        queue.insert(0, temp[temp.find('Я - ') + 4:-1])
