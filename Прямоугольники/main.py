class Rectangle:
    def __init__(self, x, y, w, h):
        self.__x = x
        self.__y = y
        self.__w = w
        self.__h = h

    def get_x(self):
        return self.__x

    def get_y(self):
        return self.__y

    def get_w(self):
        return self.__w

    def get_h(self):
        return self.__h

    def intersection(self, rect):
        if rect.get_x() >= self.__x + self.__w or rect.get_y() >= self.__y + self.__h or \
                rect.get_x() + rect.get_w() <= self.__x or self.get_y() + self.get_h() <= self.__y:
            return

        elif self.__x >= rect.get_x() and self.__x + self.__w < rect.get_x() + rect.get_w() and \
                self.__y >= rect.get_y() and self.__y + self.__h < rect.get_y() + rect.get_h():
            return Rectangle(self.__x, self.__y, self.__w, self.__h)

        elif rect.get_x() >= self.__x and rect.get_x() + self.get_w() < self.__x + self.__w and \
                rect.get_y() >= self.__y and rect.get_y() + self.get_h() < self.__y + self.__h:
            return Rectangle(rect.get_x(), rect.get_y(), rect.get_w(), rect.get_h())

        elif rect.get_x() < self.__x or rect.get_y() < self.__y:
            return Rectangle(self.__x, self.__y,
                             rect.get_w() - self.__x + rect.get_x(),
                             rect.get_h() - self.__y + rect.get_y())

        else:
            return Rectangle(rect.get_x(), rect.get_y(),
                             self.__w - rect.get_x() + self.__x, self.__h - rect.get_y() + self.__y)

