pattern = [int(el) - 1 for el in input().split()]
strings = [string.lower() for string in input().split()]
print(strings[pattern[0]].capitalize(), end=' ')
print(*[strings[el] for el in pattern[1:]])
