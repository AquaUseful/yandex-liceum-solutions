WHITE = 'white'
BLACK = 'black'


class ChessPiece:
    def __init__(self, row, col, color):
        self._first_move = True
        self._row = row
        self._col = col
        self._color = color

    def set_position(self, row, col):
        self._row = row
        self._col = col
        self._first_move = False

    def check_board(self, row, col):
        return 0 <= row < 8 and 0 <= col < 8

    def char(self):
        return self.__class__.__name__[0]

    def get_color(self):
        return self._color

    def can_move(self, row, col):
        return self.check_board(row, col)


class Queen(ChessPiece):
    def can_move(self, row, col):
        return self.check_board(row, col) and \
               (self._row == row or self._col == col or
                (abs(self._row - row) == abs(self._col - col)))


class Bishop(ChessPiece):
    def can_move(self, row, col):
        return self.check_board(row, col) and abs(self._row - row) == abs(self._col - col)


class Knight(ChessPiece):
    def char(self):
        return 'N'

    def can_move(self, row, col):
        flag = False
        for row_shift in [-2, -1, 1, 2]:
            for col_shift in [-2, -1, 1, 2]:
                if abs(row_shift) == abs(col_shift):
                    continue
                if self._row + row_shift == row and self._col + col_shift == col:
                    flag = True
                    break
            else:
                continue
            break
        return self.check_board(row, col) and flag


class Rook(ChessPiece):
    def can_move(self, row, col):
        return self.check_board(row, col) and (self._row == row or self._col == col)


class Pawn(ChessPiece):
    def can_move(self, row, col):
        direction = 1 if self._color == WHITE else -1
        return self.check_board(row, col) and self._col == col and \
            (self._row + direction == row or
                (self._first_move and self._row + 2 * direction == row))


class Board:
    def __init__(self):
        self._color = WHITE
        self.field = [[None] * 8 for _ in range(8)]

    def current_player_color(self):
        return self._color

    def cell(self, row, col):
        piece = self._field[row][col]
        return '  ' if piece is None else ('w' if color == WHITE else 'b') + piece.char()

    def move_piece(self, row, col, row1, col1):
        piece = self.field[row][col]
        if (row != row1 and col != col1) and piece.get_color() == self._color and \
                piece.can_move(row1, col1):
            self.field[row][col], self.field[row1][col1] = \
                self.field[row1][col1], self.field[row][col]
            piece.set_position(row1, col1)
            self._color = opponent(self._color)
            return True
        return False

    def is_under_attack(self, row, col, color):
        for line in self.field:
            for piece in line:
                if piece is None:
                    continue
                if piece.can_move(row, col) and piece.get_color() == color:
                    return True
        return False


def opponent(color):
    return BLACK if color == WHITE else WHITE

