def squared():
    for d in range(1, 10):
        print(*[str((d * 10 + n) ** 2).ljust(4) if n < 9 else str((d * 10 + n) ** 2)
                for n in range(1, 10)])
