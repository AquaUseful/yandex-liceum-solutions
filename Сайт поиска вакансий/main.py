class Profile:
    def __init__(self, job):
        self.__job = job

    def info(self):
        return ''

    def describe(self):
        print(self.__job + info())


class Vacancy(Profile):
    def __init__(self, job, pay):
        self.__pay = pay
        super().__init__(job)

    def info(self):
        return f'Предлагаемая зарплата: {self.__pay}'


class Resume(Profile):
    def __init__(self, job, exp):
        self.__exp = exp
        super().__init__(job)

    def info(self):
        return f'Стаж работы: {self.__exp}'

