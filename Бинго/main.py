def make_bingo():
    from random import randrange
    nums = list(range(1, 76))
    return tuple(tuple(0 if x == y == 2 else nums.pop(randrange(len(nums)))
                       for x in range(5)) for y in range(5))
