cities = set()
for i in range(int(input()) + 1):
    temp = input()
    if temp in cities:
        print('TRY ANOTHER')
        break
    else:
        cities.add(temp)
else:
    print('OK')