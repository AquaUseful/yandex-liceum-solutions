values = [int(el) for el in input().split()]
print('*' * (len(values) + 2) + '\n' + '*' + ' ' * len(values) + '*')
print(*['*' + ''.join([('*' if col <= val else ' ') for val in values])
        + '*' for col in range(max(values), 0, -1)], sep='\n')
