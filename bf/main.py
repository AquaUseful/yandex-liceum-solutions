mem = [0] * 30000
p = 0


def find_loop_line(line):
    for el in range(len(line)):
        if line[el] == ']':
            if line[:el].count('[') == line[:el].count(']'):
                return line[:el]


def op(cmd):
    global p, mem
    if cmd == '.':
        print(mem[p])
    elif cmd == '>':
        p += 1
    elif cmd == '<':
        p -= 1
    elif cmd == '+':
        mem[p] += 1
    elif cmd == '-':
        mem[p] -= 1
    p = p % 30000
    mem[p] = mem[p] % 256


def loop(cmdl):
    global p, mem
    while mem[p] != 0:
        exec_line(cmdl)


def exec_line(cmdLine):
    cmdPos = 0
    while cmdPos < len(cmdLine):
        if cmdLine[cmdPos] == '[':
            loop(find_loop_line(cmdLine[cmdPos + 1:]))
            cmdPos += len(find_loop_line(cmdLine[cmdPos + 1:]))
        else:
            op(cmdLine[cmdPos])
        cmdPos += 1


exec_line(input())

