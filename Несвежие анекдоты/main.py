printedStrings = []


def print_only_new(message):
    global printedStrings
    if message not in printedStrings:
        print(message)
        printedStrings.append(message)
