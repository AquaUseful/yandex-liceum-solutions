SERVERS = []
USERS = []


class Server:
    def __init__(self, ip):
        self.ip = ip
        self.letters = []
        self.users = []
        SERVERS.append(self)

    def get_ip(self):
        return self.ip

    def get_users(self):
        return self.users

    def print_users_on_server(self):
        [print(i.name) for i in self.users]

    def print_letters(self):
        for i in self.letters:
            print(i[0].name, i[1])


class User:
    def __init__(self, name):
        self.name = name
        USERS.append(self)

    def return_name_user(self):
        return self.name


class MailClient:
    def __init__(self, server, user):
        self.server = server
        self.user = user
        if server not in SERVERS:
            print('Не существует такого сервера.')
            return
        elif user not in USERS:
            print('Не существует такого пользователя.')
        else:
            server.users.append(user)

    def receive_mail(self):
        j = 0
        for i in self.server.letters:
            if i[0] == self.user:
                print(self.user.return_name_user(), ':', i[1])
                self.server.letters.pop(j)
            j += 1

    def send_mail(self, server, user, message):
        server.letters.append([user, message])


def main():
    while True:
        current_mail_client = None
        print('Меню почты:')
        print('1 : Создать новый сервер')
        print('2 : Создать нового пользователя')
        print('3 : Подключение пользователя к серверу')
        print('4 : Отправить сообщение пользователю на сервере')
        print('5 : Получь почту')
        n = int(input())
        if n > 5 or n < 1:
            print('Нет такого пункта!')
        else:
            if n == 1:
                print('Введите IP сервера:')
                ip = input()
                Server(ip)
            elif n == 2:
                print('Введите имя пользователя:')
                name = input()
                User(name)
            elif n == 3:
                print('Введите адрес сервера и имя пользователя:')
                user = input()
                ip_user = input()
                temp_server = None
                temp_user = None
                for i in SERVERS:
                    if i.get_ip() == ip_user:
                        temp_server = i
                        break
                for i in USERS:
                    if i.return_name_user() == user:
                        temp_user = i
                        break
                if temp_user is None or temp_server is None:
                    print('Не найден сервер или имя пользователя.')
                    continue
                else:
                    nonlocal current_mail_client
                    current_mail_client = MailClient(temp_server, temp_user)
            elif n == 4:
                print('Введите адрес сервера, имя пользователя и сообщение(всё на разных строчках)')
                temp_server = None
                temp_user = None
                address = input()
                user = input()
                mes = input()
                for i in SERVERS:
                    if i.get_ip() == address:
                        temp_server = i
                        break
                for i in USERS:
                    if i.return_name_user() == user:
                        temp_user = i
                        break
                if temp_user is None or temp_server in None:
                    print('Не найден сервер или имя пользователя.')
                    continue
                else:
                    current_mail_client.send_mail(temp_server, temp_user, mes)
            elif n == 5:
                print(current_mail_client.receive_mail())


# s1 = Server('123')
# s2 = Server('456')
# u1 = User('max')
# u2 = User('andrey')
# u3 = User('dima')
# c1 = MailClient(s1, u1)
# c2 = MailClient(s1, u2)
# c3 = MailClient(s2, u3)
#
# print(s1.get_users())
# print(s2.get_users())
#
# s1.print_users_on_server()
# print()
# s2.print_users_on_server()
#
# print()
#
# c1.send_mail(s2, u3, 'hello')
# c3.send_mail(s1, u1, 'lol')
# c3.send_mail(s1, u2, 'kek')
#
# s1.print_letters()
# print()
# s2.print_letters()
#
# c1.receive_mail()
# c2.receive_mail()
# c3.receive_mail()
#
# print('test')
# print(s1.letters)
# print(s2.letters)