book = {}
for _ in range(int(input())):
    temp = input().split()
    book[temp[-1]] = book.get(temp[-1], []) + [temp[0]]
print(*[' '.join(book[name]) if name in book else 'Нет в телефонной книге'
        for name in [input() for _ in range(int(input()))]], sep='\n')
