string = input().lower()
print(max([string.count(letter) for letter in set(string)]))
