x, y = int(input()), int(input())
lst = [[input() for _ in range(y)] for _ in range(x)]
for el in lst:
    print(*el, sep='\t')
print()
for el in [[lst[x][y] for x in range(0, len(lst))] for y in range(len(lst[0]))]:
    print(*el, sep='\t')
