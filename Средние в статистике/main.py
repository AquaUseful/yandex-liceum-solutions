values = sorted([float(val) for val in input().split()])
print(sum(values) / len(values), (values[len(values) // 2] + values[len(values) // 2 - 1]) / 2
      if len(values) % 2 == 0 else values[len(values) // 2])
