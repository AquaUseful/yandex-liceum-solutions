def encrypt_caesar(msg, shift=3):
    alphabetLower = 'абвгдежзийклмнопрстуфхцчшщъыьэюя'
    alphabetUpper = alphabetLower.upper()
    code = ''
    for char in msg:
        if char not in alphabetLower + alphabetUpper:
            code += char
            continue
        if ord(char) + shift > ord('я') and char in alphabetLower:
            code += chr(ord(char) + shift - 32)
        elif ord(char) + shift > ord('Я') and char in alphabetUpper:
            code += chr(ord(char) + shift - 32)
        elif ord(char) + shift < ord('а') and char in alphabetLower:
            code += chr(ord(char) + shift + 32)
        elif ord(char) + shift < ord('А') and char in alphabetUpper:
            code += chr(ord(char) + shift + 32)
        else:
            code += chr((ord(char) + shift))
    return code


def decrypt_caesar(msg, shift=3):
    return encrypt_caesar(msg, -shift)
