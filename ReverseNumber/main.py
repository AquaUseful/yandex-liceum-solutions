num = float(input())
if abs(num) < 0.000001:
    print(1000000)
else:
    print(num ** -1)