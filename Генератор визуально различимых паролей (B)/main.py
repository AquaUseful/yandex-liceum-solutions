from string import ascii_lowercase, digits, ascii_uppercase
LETTERS_LOWER = ''.join((map(lambda el: '' if el in 'lI10oO' else el, ascii_lowercase)))
LETTERS_UPPER = ''.join((map(lambda el: '' if el in 'lI10oO' else el, ascii_uppercase)))
DIGITS = ''.join((map(lambda el: '' if el in 'lI10oO' else el, digits)))
ALPHABET = LETTERS_LOWER + LETTERS_UPPER + DIGITS


def randpop(lst):
    from random import randrange
    return lst.pop(randrange(len(lst)))


def generate_password(m):
    from random import choice
    rand = list(range(m))
    pswd = list(map(lambda a: choice(ALPHABET), range(m)))
    for tst in [DIGITS, LETTERS_UPPER, LETTERS_LOWER]:
        if any(map(lambda el: el not in pswd, tst)):
            pswd[randpop(rand)] = choice(tst)
    return ''.join(pswd)


def main(n, m):
    passwords = []
    while len(passwords) < n:
        password = generate_password(m)
        if password not in passwords:
            passwords.append(password)
    return passwords
