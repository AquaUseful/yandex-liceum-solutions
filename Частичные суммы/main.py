def partial_sums(*values):
    return [sum(values[:i]) for i in range(len(values) + 1)]
