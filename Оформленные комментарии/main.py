from sys import stdin
lc = 1
for line in stdin:
    line = line.strip()
    if not line:
        continue
    if line[0] == '#':
        print(f'Line {lc}: {line[1:].strip()}')
    lc += 1
