def linear(some_list):
    if not some_list:
        return []
    if type(some_list[0]) == list:
        return linear(some_list[0]) + linear(some_list[1:])
    return [some_list[0]] + linear(some_list[1:])
