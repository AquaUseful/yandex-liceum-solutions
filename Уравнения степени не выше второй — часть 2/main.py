def solve(*coeffecients):
    from math import sqrt
    if not coeffecients or len(coeffecients) > 3:
        return
    a, b, c = [0] * (3 - len(coeffecients)) + list(coeffecients)
    if a == b == c == 0:
        return ['all']
    elif a == b == 0 and c != 0:
        return []
    elif a == 0:
        return [-c / b]
    else:
        d = b ** 2 - 4 * a * c
        if d < 0:
            return []
        elif d == 0:
            return [-b / (2 * a)]
        else:
            return [(-b - sqrt(d)) / (2 * a), (-b + sqrt(d)) / (2 * a)]
