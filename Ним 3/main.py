def prog_take():
    global heaps
    nim = heaps[0] ^ heaps[1] ^ heaps[2]
    if nim > 0:
        for pos in range(3):
            if heaps[pos] ^ nim < heaps[pos]:
                take = heaps[pos] - (heaps[pos] ^ nim)
                break
    else:
        take = 1
        pos = 0
    heaps[pos] -= take
    print('Программа берет', take, 'камней из', pos + 1, 'кучи')


def user_take():
    while True:
        pos = int(input('Из какой кучи вы хотите взять камень?\n')) - 1
        if pos in (0, 1, 2) and heaps[pos] > 0:
            break
        print('Неверный номер кучи или в куче больше нет камней!')
    while True:
        take = int(input('Сколько камней вы хотите взять? (Не больше ' + str(heaps[pos]) + ')\n'))
        if take <= heaps[pos]:
            break
        print('Вы не можете взять больше камней, чем есть в куче!')
    heaps[pos] -= take
    print('Вы взяли', take, 'камней из кучи', pos + 1)


def print_heaps():
    print('Осталось камней в кучах:', ' '.join([str(heap) for heap in heaps]))


heaps = [int(val) for val in input('Введите количество камнней в кучах (через пробел)\n').split()]

while True:
    prog_take()
    print_heaps()
    if sum(heaps) == 0:
        print('Программа выиграла!')
        break
    user_take()
    print_heaps()
    if sum(heaps) == 0:
        print('Вы выиграли!')
        break
