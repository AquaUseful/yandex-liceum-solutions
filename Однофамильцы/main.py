surnSet = set()
surnList = []
for _ in range(int(input())):
    temp = input()
    surnSet.add(temp)
    surnList.append(temp)
count = 0
for surname in surnSet:
    if surnList.count(surname) > 1:
        count += surnList.count(surname)
print(count)