def translate(text):
    global translatedText
    blacklist = '''уеыаоэяиюё,.?'";:-'''
    text = ''.join([char for char in text if char.lower() not in blacklist])
    translatedText = ' '.join(text.split())



