from math import sqrt, sin, cos, pi


def float_range(start, stop, step):
    num = start
    while num < stop:
        yield num
        num += step


print(min(map(lambda coords: sqrt((0.75 - coords[0]) ** 2 + (-coords[1]) ** 2),
              map(lambda t: (cos(t) ** 3, sin(t) ** 3), float_range(0, 2 * pi, 0.0001)))))
