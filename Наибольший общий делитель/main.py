from functools import reduce
from math import gcd
from sys import stdin
print(reduce(gcd, map(int, stdin.read().split())))

