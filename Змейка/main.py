def snake(rows, cols):
    from itertools import count, cycle, islice
    import numpy as np
    counter = count(1)
    step_switch = cycle((1, -1))
    matr = []
    for _ in range(rows):
        row = islice(counter, cols)
        matr.append(tuple(islice(counter, cols))[::next(step_switch)])
    return np.array(matr)

