import tkinter


def show_key(event):
    keysym.config(text='keysum: ' + event.keysym)
    char.config(text='char: ' + event.char)
    keysymNum.config(text='keysym_num: ' + str(event.keysym_num))
    keycode.config(text='keycode: ' + str(event.keycode))


master = tkinter.Tk()

keysym = tkinter.Label(master, text='keysym')
char = tkinter.Label(master, text='char')
keysymNum = tkinter.Label(master, text='keysum_num')
keycode = tkinter.Label(master, text='keycode')

keysym.pack()
char.pack()
keysymNum.pack()
keycode.pack()

master.bind("<KeyPress>", show_key)
master.mainloop()
