def makeanagliph(filename, delta):
    from PIL import Image, ImageChops, ImageDraw
    img = Image.open(filename)
    empty = Image.new('L', img.size, 0)
    copy1, copy2 = img.copy(), img.copy()
    copy1 = Image.merge('RGB', (copy1.getchannel(0), empty, empty))
    copy2 = Image.merge('RGB', (empty, copy2.getchannel(1), copy2.getchannel(2)))
    copy1 = ImageChops.offset(copy1, delta, 0)
    dr = ImageDraw.Draw(copy1)
    dr.rectangle((0, 0, delta - 1, copy1.size[1]), fill='black')
    del dr
    img = ImageChops.add(copy1, copy2)
    img.save('res.jpg')
