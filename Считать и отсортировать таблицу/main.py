x, y = int(input()), int(input())
lst = [sorted([input() for _ in range(y)]) if row != 0 and row != x - 1 else
       [input() for _ in range(y)] for row in range(x)]
for el in lst:
    print(*el, sep='\t')
