def random_format(string, **kwargs):
    from random import randint
    for item in kwargs.items():
        while item[0] in string:
            string = string.replace(item[0], str(randint(*item[1])), 1)
    return string

