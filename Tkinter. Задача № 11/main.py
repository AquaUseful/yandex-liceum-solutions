import tkinter
import random


def prepare_and_start():
    global player, exit, fires, enemies
    canvas.delete("all")
    player_pos = (random.randint(1, N_X - 1) * step,
                  random.randint(1, N_Y - 1) * step)
    exit_pos = (random.randint(1, N_X - 1) * step,
                random.randint(1, N_Y - 1) * step)
    player = canvas.create_oval(
        (player_pos[0], player_pos[1]),
        (player_pos[0] + step, player_pos[1] + step),
        fill='green'
    )
    exit = canvas.create_oval(
        (exit_pos[0], exit_pos[1]),
        (exit_pos[0] + step, exit_pos[1] + step),
        fill='black'
    )
    N_FIRES = 100  # Число клеток, заполненных огнем
    fires = []
    for i in range(N_FIRES):
        fire_pos = (random.randint(1, N_X - 1) * step,
                    random.randint(1, N_Y - 1) * step)
        fire = canvas.create_rectangle(
            (fire_pos[0], fire_pos[1]),
            (fire_pos[0] + step, fire_pos[1] + step),
            fill='orange'
        )
        fires.append(fire)
        N_ENEMIES = 10  # Число врагов
    enemies = []
    for i in range(N_ENEMIES):
        enemy_pos = (random.randint(1, N_X - 1) * step,
                     random.randint(1, N_Y - 1) * step)
        enemy = canvas.create_oval(
            (enemy_pos[0], enemy_pos[1]),
            (enemy_pos[0] + step, enemy_pos[1] + step),
            fill='red'
        )
        enemies.append((enemy, random.choice([always_right, random_move])))
    label.config(text="Найди выход")
    master.bind("<KeyPress>", key_pressed)


def move_wrap(canvas, obj, move):
    canvas.move(obj, move[0], move[1])

    if canvas.coords(obj)[0] >= N_X * step:
        canvas.move(obj, -N_X * step, 0)
    elif canvas.coords(obj)[0] < 0:
        canvas.move(obj, N_X * step, 0)

    if canvas.coords(obj)[1] >= N_Y * step:
        canvas.move(obj, 0, -N_Y * step)
    elif canvas.coords(obj)[1] < 0:
        canvas.move(obj, 0, N_Y * step)


def key_pressed(event):
    if event.keysym == 'Up':
        move_wrap(canvas, player, (0, -step))
    elif event.keysym == 'Down':
        move_wrap(canvas, player, (0, step))
    elif event.keysym == 'Right':
        move_wrap(canvas, player, (step, 0))
    elif event.keysym == 'Left':
        move_wrap(canvas, player, (-step, 0))
    for enemy in enemies:
        direction = enemy[1]()  # вызвать функцию перемещения у "врага"
        move_wrap(canvas, enemy[0], direction)  # произвести  перемещение.
    check_move()


def do_nothing(useless):
    return


def check_move():
    if canvas.coords(player) == canvas.coords(exit):
        label.config(text="Победа!")
        master.bind("<KeyPress>", do_nothing)
    for f in fires:
        if canvas.coords(player) == canvas.coords(f):
            label.config(text="Ты проиграл!")
            master.bind("<KeyPress>", do_nothing)
    for e in enemies:
        if canvas.coords(player) == canvas.coords(e[0]):
            label.config(text="Ты проиграл!")
            master.bind("<KeyPress>", do_nothing)


def always_right():
    return (step, 0)


def random_move():
    return random.choice([(step, 0), (-step, 0), (0, step), (0, -step)])


step = 30  # Размер клетки
N_X = 20
N_Y = 20   # Размер сетки
master = tkinter.Tk()
label = tkinter.Label(master, text="")
label.pack()
canvas = tkinter.Canvas(
    master, bg='blue', height=N_X * step, width=N_Y * step)
canvas.pack()
restart = tkinter.Button(master, text="Начать заново",
                         command=prepare_and_start)
restart.pack()
prepare_and_start()
master.mainloop()