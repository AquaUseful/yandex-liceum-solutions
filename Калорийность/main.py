class FoodInfo:
    def __init__(self, proteins, fats, carbohydrates):
        self.__proteins = proteins
        self.__fats = fats
        self.__carbohydrates = carbohydrates

    def get_proteins(self):
        return self.__proteins

    def get_fats(self):
        return self.__fats

    def get_carbohydrates(self):
        return self.__carbohydrates

    def get_kcalories(self):
        return 4 * self.__proteins + 9 * self.__fats + 4 * self.__carbohydrates

    def __add__(self, other):
        return FoodInfo(self.__proteins + other.get_proteins(),
                        self.__fats + other.get_fats(),
                        self.__carbohydrates + other.get_carbohydrates())

