dic = {}
for _ in range(int(input())):
    temp = input().split()
    dic[temp[-1]] = sorted(sorted(dic.get(temp[-1], []) + [' '.join(temp[:2])],
                                  key=lambda k: k.split()[0]), key=lambda k: int(k.split()[1]))
print(*[' '.join(dic.get(input(), '')) for _ in range(int(input()))], sep='\n')
