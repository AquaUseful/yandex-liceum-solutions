# Делайте ставки и зарабатывайте на 1xbet

avaliableHorses = set(range(1, 11))


def do_bet(horse, bet):
    global avaliableHorses
    if horse not in avaliableHorses or bet <= 0:
        print('Что-то пошло не так, попробуйте еще раз')
        return
    print(f'Ваша ставка в размере {bet} на лошадь {horse} принята')
    avaliableHorses.remove(horse)
