seq = [0]
for _ in range(int(input()) - 1):
    matches = 0
    for el in range(len(seq)):
        if seq[el] == seq[::-1][el]:
            matches += 1
    seq.append(matches)
print(*seq, sep='\n')
