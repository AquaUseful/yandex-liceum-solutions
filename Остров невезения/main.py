d = int(input())
m = int(input()) - 2
y = int(input())
if m <= 0:
    y -= 1
    m = 12 + m
c = y // 100
y = y % 100
magic = d + ((13 * m - 1) // 5) + y + (y // 4 + c // 4 - 2 * c + 777)
print(magic % 7)