from sys import stdin
print(*sorted(map(lambda st: st.strip(), stdin),
              key=lambda st: ((lambda i: sum(map(lambda ch: ord(ch) - ord('A') + 1, i.upper())))(st),
                              st)), sep='\n')
