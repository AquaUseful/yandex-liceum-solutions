valSets = [valSet.split() for valSet in input().split('|')]
out = []
for valSet in valSets:
    flag = True
    for currChar in range(len(valSet[0])):
        for char in range(len(valSet[0])):
            if currChar == char:
                continue
            if valSet[0][currChar] == valSet[0][char]:
                flag = False
    if not(int(valSet[1]) <= int(valSet[3]) <= int(valSet[2])):
        flag = False
    if valSet[4].count('1') != valSet[4].count('0'):
        flag = False
    if flag:
        out.append(' '.join(valSet))
print(*out, sep='|')
