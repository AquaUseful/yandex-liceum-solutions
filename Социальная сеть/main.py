class User:
    def __init__(self, name):
        self._name = name

    def send_message(self, user, message):
        pass

    def post(self, message):
        pass

    def info(self):
        return ''

    def describe(self):
        print(name, self.info())


class Person(User):
    def __init__(self, name, birthday):
        self._name = name
        self._birthday = birthday

    def info(self):
        return f'Дата рождения: {self._birthday}'

    def subscribe(self, user):
        pass


class Community(User):
    def __init__(self, name, description):
        self._name = name
        self._description = description

    def info(self):
        return f'Описание: {self._description}'

