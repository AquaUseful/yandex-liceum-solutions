allowed = 'qwertyuiopasdfghjklzxcvbnm0123456789_'
for char in input():
    if char not in allowed:
        print('Неверный символ:', char)
        break
else:
    print('OK')