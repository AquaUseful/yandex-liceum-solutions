data = input()
words = data.split()
dic = {key: 0 for key in [char for char in data if char.isdigit()]}
for key in dic:
    for el in words:
        if el[-1] == key:
            dic[key] += 1
print(dic)
